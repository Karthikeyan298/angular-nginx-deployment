# Use the official Nginx base image
FROM node:18-alpine
WORKDIR /app
COPY . /app/
RUN npm run build

FROM nginx:latest

EXPOSE 80

COPY nginx.default.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=0 /app/dist/* /usr/share/nginx/html/.

ENV CONTEXT_PATH http://localhost:80/
ENV BASE_PATH /

CMD \
  mainFiles=$(ls /usr/share/nginx/html/main*.js) \
  && \
  for f in ${mainFiles}; do \
    envsubst '${ENDPOINT},${VARIABLE}' < "$f" > "${f}.tmp" && mv "${f}.tmp" "$f"; \
  done \
  && \
  runtimeFiles=$(grep -lr "custom-host-name" /usr/share/nginx/html) \
  && \
  for f in ${runtimeFiles}; do sed -i -E "s@http://custom-host-name/?@${CONTEXT_PATH}@g" "$f"; done \
  && \
  sed -i -E "s@custom-host-name@${BASE_PATH}@g" /etc/nginx/conf.d/default.conf \
  && \
  nginx -g 'daemon off;'

